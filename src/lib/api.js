import axios from '../../node_modules/axios'

const key = 'AIzaSyDIf5Z6gI3SP3sFF2iNjwefc-8oPC-I6kk'
const fetcher = axios.create({
    baseURL: 'https://youtube.googleapis.com/youtube/v3',
    params: {
        key: key
    }
})

export default fetcher;